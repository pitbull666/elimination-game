﻿#include<stdlib.h>
#include <stdio.h>
#include"Declare.h"
#include<graphics.h>
#pragma comment(lib,"Winmm.lib")
extern int steps = 10;
extern int targetscores = 1000;
extern int is_success=1;
extern void save();
MOUSEMSG cur;
IMAGE img_menu_start, img_menu_start1,img_menu_continue, img_menu_continue1,img_menu_background,
img_menu_exit1, img_menu_exit,img_menu_title, img_menu_title1;
FILE* fp;
extern int level = 1;
int* p = &level;

void menu(){
	loadimage(&img_menu_background, "..\\Images\\menubackground.jpg",1000,650);
	putimage(0,0, &img_menu_background);
	loadimage(&img_menu_title, "..\\Images\\title1.png",0,300);
	putimage(250, 0, &img_menu_title, NOTSRCERASE);
	loadimage(&img_menu_title1, "..\\Images\\title.png",0,300);
	putimage(250, 0, &img_menu_title1, SRCINVERT);
	loadimage(&img_menu_start1, "..\\Images\\start1.png");
	putimage(380, 300, &img_menu_start1, NOTSRCERASE);
	loadimage(&img_menu_start, "..\\Images\\start.png");
	putimage(380, 300, &img_menu_start, SRCINVERT);
	loadimage(&img_menu_continue1, "..\\Images\\continue1.png", 238, 80);
	putimage(380, 400, &img_menu_continue1, NOTSRCERASE);
	loadimage(&img_menu_continue, "..\\Images\\continue.png",238,80);
	putimage(380, 400, &img_menu_continue, SRCINVERT);
	loadimage(&img_menu_exit1, "..\\Images\\menuexit1.png", 238,150);
	putimage(370, 480, &img_menu_exit1, NOTSRCERASE);
	loadimage(&img_menu_exit, "..\\Images\\menuexit.png", 238,150);
	putimage(370, 480, &img_menu_exit, SRCINVERT);
	mciSendString("close backmusic", NULL, 0, NULL);
	mciSendString("open ..\\Music\\menubackground.mp3 alias menumusic", NULL, 0, NULL);
	mciSendString("play  menumusic", NULL, 0, NULL);
}
void setdifficulty(int n) {
	if (n <= 5) {
		targetscores = 1000 + (n - 1) * 500;
		steps = 10;
	}
	else if (n > 5 && n <= 10) {
		targetscores = 1000 + (n - 1) * 1000;
		steps = 15;
	}
	else if (n > 10 && n <= 15) {
		targetscores = 1000 + (n - 1) * 1500;
		steps = 20;
	}
	else if (n > 15 && n <= 20) {
		targetscores = 1000 + (n - 1) * 2000;
		steps = 30;
	}
	if (n > 20) steps = 1000;
}
extern void save() {
	fopen_s(&fp, "data.dat", "wb") ;
	if (fp) {
		fwrite(p, sizeof(int), 1, fp);
		fclose(fp);
	}
}

int main() {
	init();
	menu();
	while (1) {
		cur = GetMouseMsg();
		if ((cur.uMsg == WM_LBUTTONDOWN) && cur.x>=380 && cur.x<=618 && cur.y>=280 && cur.y<=360) {
			steps = 10;
			targetscores = 1000;
			level = 1;
			init();
			begin();
			play();
			while (is_success) {
				level++;
				save();
				init();
				setdifficulty(level);
				begin();
				play();
			}
			save();
			menu();
			steps = 10;
			targetscores = 1000;
		}
		else if ((cur.uMsg == WM_LBUTTONDOWN) && cur.x >= 380 && cur.x <= 618 && cur.y >= 370 && cur.y <= 450) {
			fopen_s(&fp, "data.dat", "rb");
			if(fp){
				fread(p, sizeof(int), 1, fp);
				fclose(fp);
			}
			init();
			setdifficulty(level);
			begin();
			play();
			while (is_success) {
				level++;
				save();
				init();
				setdifficulty(level);
				begin();
				play();
			}
			save();
			menu();
			steps = 10;
			targetscores = 1000;
		}
		else if ((cur.uMsg == WM_LBUTTONDOWN) && cur.x >= 380 && cur.x <= 618 && cur.y >= 460 && cur.y <= 540) {
			break;
		}
	
	}
}