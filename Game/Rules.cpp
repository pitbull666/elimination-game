#include <stdio.h>
#include<stdlib.h>
#include<graphics.h>
#include<windows.h>
#include<time.h>
#pragma comment(lib,"Winmm.lib")
struct Position {
	int x;
	int y;
};
MOUSEMSG m;
COLORREF color[6] = { RED,BLUE,YELLOW,GREEN,LIGHTGRAY,RGB(128, 0, 128) };
Position Balls[50];
IMAGE background,screen,success, success1,successback,fail,fail1,failback;
extern int steps;
extern int targetscores;
extern int is_success;
extern void save();
extern int level;

int index = 0;
int score = 0;
int number = 0;

void ShowSteps(int steps);
void ShowScores(int scores);
void SetRandBackGroundMusic(int n);
void SetRandBackGroundImage(int n);
Position GetCenter(MOUSEMSG m);
int IsRepeated(Position cur, COLORREF c);
int GetSameColor(Position m, COLORREF c);
void CalculateScores(COLORREF c, int num);
int IsRepeated(Position cur, COLORREF c);
int GetSameColor(Position m, COLORREF c);
void BallFalls();
void PrintTips();


void init() {
	initgraph(1000, 650);
	setbkcolor(WHITE);
}
void SetRandBackGroundMusic(int n) {
	const char s[5][50] = {"open ..\\Music\\background2.mp3 alias backmusic","open ..\\Music\\background2.mp3 alias backmusic","open ..\\Music\\background3.mp3 alias backmusic",
	"open ..\\Music\\background4.mp3 alias backmusic","open ..\\Music\\background5.mp3 alias backmusic"};
	mciSendString("close backmusic", NULL, 0, NULL);
	mciSendString(s[n], NULL, 0, NULL);
	mciSendString("play backmusic repeat", NULL, 0, NULL);
}
void SetRandBackGroundImage(int n) {
	const char s[10][40] = { "..\\Images\\background1.png","..\\Images\\background2.png","..\\Images\\background3.png","..\\Images\\background4.png","..\\Images\\background5.png",
		"..\\Images\\background6.png" ,"..\\Images\\background7.png","..\\Images\\background8.png","..\\Images\\background9.png","..\\Images\\background10.png"};
	loadimage(&background,s[n],1110,720);
	putimage(-50, -20, &background);
}
void PrintTips() {
	setlinecolor(RED);
	setfillcolor(RED);
	fillcircle(850,70,20);
	setlinecolor(BLUE);
	setfillcolor(BLUE);
	fillcircle(850,170,20);
	setlinecolor(YELLOW);
	setfillcolor(YELLOW);
	fillcircle(850,270,20);
	setlinecolor(GREEN);
	setfillcolor(GREEN);
	fillcircle(850,370,20);
	setlinecolor(LIGHTGRAY);
	setfillcolor(LIGHTGRAY);
	fillcircle(850,470,20);
	setlinecolor(RGB(128, 0, 128));
	setfillcolor(RGB(128, 0, 128));
	fillcircle(850,570,20);
	settextcolor(BLACK);
	settextstyle(25, 0, _T("黑体"));
	outtextxy(830,10,"分值：");
	settextcolor(BLACK);
	settextstyle(25, 0, _T("黑体"));
	outtextxy(880,60,"×5");
	settextstyle(25, 0, _T("黑体"));
	outtextxy(880,160,"×10");
	settextstyle(25, 0, _T("黑体"));
	outtextxy(880,260,"×20");
	settextstyle(25, 0, _T("黑体"));
	outtextxy(880,360,"×50");
	settextstyle(25, 0, _T("黑体"));
	outtextxy(880,460, "×100");
	settextstyle(25, 0, _T("黑体"));
	outtextxy(880,560,"×200");
}
void ShowSteps(int steps) {
	char s[20];
	setbkcolor(WHITE);
	if (steps >= 5) {
		settextcolor(BLACK);
		settextstyle(25, 0, _T("黑体"));
		sprintf_s(s, "剩余步数：%d ", steps);
		outtextxy(20, 80, s);
	}
	else {
		settextcolor(RED);
		settextstyle(25, 0, _T("黑体"));
		sprintf_s(s, "剩余步数：%d ", steps);
		outtextxy(20, 80, s);
	}
}
void ShowScores(int scores) {
	setbkcolor(WHITE);
	char s[20];
	settextcolor(BLACK);
	settextstyle(25, 0, _T("黑体"));
	sprintf_s(s, "分数：%d", scores);
	outtextxy(50, 480, s);
	sprintf_s(s, "目标分数：%d", targetscores);
	outtextxy(10, 440, s);
	sprintf_s(s, "当前关卡：%d",level);
	outtextxy(30, 270, s);


}
void begin() {
	save();
	setlinecolor(RGB(50, 50, 50));
	setlinestyle(PS_SOLID | PS_JOIN_ROUND, 5);
	rectangle(200, 40, 808, 610);
	setlinestyle(PS_SOLID);
	srand((unsigned)time(NULL));
	SetRandBackGroundImage(rand()%10);
	loadimage(&screen, "..\\Images\\screen.png",610,575);
	putimage(200, 40, &screen);
	srand((unsigned)time(NULL));
	int x, y;
	for (x = 225; x < 800; x += 40)
	{
		for (y = 65; y < 600; y += 40)
		{
			COLORREF c = color[rand() % 6];
			setlinecolor(c);
			setfillcolor(c);
			fillcircle(x, y, 20);
		}
	}
	ShowSteps(steps);
	ShowScores(0);
}
Position GetCenter(MOUSEMSG m) {
	if (m.x >= 205 && m.x <=805 && m.y >= 45 && m.y <= 605) {
		int i = (m.x - 205) / 40;
		int j = (m.y - 45) / 40;
		Position temp;
		temp.x = 225 + i * 40;
		temp.y = 65 + j* 40;
		return temp;
	}
}
int IsRepeated(Position cur, COLORREF c) {
	if (getpixel(cur.x, cur.y) != c) {
		return 0;
	}
	else {
		for (int i = 0; i < index; i++) {
			if (cur.x == Balls[i].x && cur.y == Balls[i].y) {
				return 0;
			}
		}
		return 1;
	}
}
int GetSameColor(Position m, COLORREF c) {
	if (c != BLACK) {
		Balls[index].x = m.x;
		Balls[index].y = m.y;
		index++;
		Position temp;
		for (int k = 0; k < 4; k++) {
			switch (k) {
			case 0:
				temp.x = m.x;
				temp.y = m.y - 40;
				break;
			case 1:
				temp.x = m.x;
				temp.y = m.y + 40;
				break;
			case 2:
				temp.x = m.x - 40;
				temp.y = m.y;
				break;
			case 3:
				temp.x = m.x + 40;
				temp.y = m.y;
				break;
			}
			if (IsRepeated(temp, c)) {
				GetSameColor(temp, c);
			}
		}
		if (index == 1) return 0;
		else return 1;
	}
	else {
		return 0;
	}
}
void CalculateScores(COLORREF c, int num) {
	if (c == RED) score = score + 5 * num;
	else if (c == BLUE) score = score + 10 * num;
	else if (c == YELLOW) score = score + 20 * num;
	else if (c == GREEN) score = score + 50 * num;
	else if (c == LIGHTGRAY) score = score + 100 * num;
	else if (c == RGB(128, 0, 128)) score = score + 200 * num;
}
void BallFalls() {
	int i, j; Position temp;
	for (i = 0; i < index - 1; i++) {
		for (j = 0; j < index - 1 - i; j++) {
			if (Balls[j].x > Balls[j + 1].x)
			{
				temp = Balls[j];
				Balls[j] = Balls[j + 1];
				Balls[j + 1] = temp;
			}
			if (Balls[j].y > Balls[j + 1].y)
			{
				temp = Balls[j];
				Balls[j] = Balls[j + 1];
				Balls[j + 1] = temp;
			}
		}
	}
	for (i = 0; i < index; i++)
	{
		for (j = Balls[i].y; j > 65; j -= 40)
		{
			COLORREF c1 = getpixel(Balls[i].x, j - 40);
			setlinecolor(c1);
			setfillcolor(c1);
			fillcircle(Balls[i].x, j, 20);
		}
		COLORREF c2 = color[rand() % 6];
		setlinecolor(c2);
		setfillcolor(c2);
		fillcircle(Balls[i].x, 65, 20);
	}
}
void play() {
	save();
	int flag = 0;
	score=0;
	srand((unsigned)time(NULL));
	SetRandBackGroundMusic(rand()%5);
	mciSendString("close menumusic", NULL, 0, NULL);
	while (1 && steps!=1000) {
		save();
		ShowSteps(steps);
		PrintTips();
		m = GetMouseMsg();
		if ((m.uMsg == WM_LBUTTONDOWN) && m.x >= 205 && m.x <= 805 && m.y >= 45 && m.y <= 605) {
			Position a;
			COLORREF temp;
			a = GetCenter(m);
			index = 0;
			temp = getpixel(a.x, a.y);
			if (GetSameColor(a, temp)) {
				mciSendString("close clkmusic", NULL, 0, NULL);
				mciSendString("open ..\\Music\\click.mp3 alias clkmusic", NULL, 0, NULL);
				mciSendString("play clkmusic", NULL, 0, NULL);
				for (int j = 0; j < index; j++) {
					setlinecolor(RGB(0, 0, 0));
					setfillcolor(RGB(0, 0, 0));
					fillcircle(Balls[j].x, Balls[j].y, 20);
				}
				Sleep(200);
				BallFalls();
				CalculateScores(temp , index);
				ShowScores(score);
				steps--;
			}
			else{
				mciSendString("close clkmusic", NULL, 0, NULL);
				mciSendString("open ..\\Music\\clickerror.mp3 alias clkmusic", NULL, 0, NULL);
				mciSendString("play clkmusic", NULL, 0, NULL);
				steps--;
			}
		}
		if (score >= targetscores) {
			flag = 1;
			break;
		}
		else if (steps == 0) {
			break;
		}
	}
	if (flag == 1 && steps!=1000) {
		save();
		is_success = 1;
		cleardevice();
		loadimage(&successback, "..\\Images\\successback.png",1110,680);
		putimage(-20, -10, &successback);
		loadimage(&success1, "..\\Images\\success1.png");
		putimage(300, 280, &success1, NOTSRCERASE);
		loadimage(&success, "..\\Images\\success.png");
		putimage(300, 280, &success, SRCINVERT);
		mciSendString("close backmusic", NULL, 0, NULL);
		mciSendString("close successmusic", NULL, 0, NULL);
		mciSendString("open ..\\Music\\success.mp3 alias successmusic", NULL, 0, NULL);
		mciSendString("play  successmusic", NULL, 0, NULL);
		settextcolor(YELLOW);
		setbkmode(TRANSPARENT);
		settextstyle(30, 0, _T("黑体"));
		outtextxy(360, 350, "5秒后进入下一关...");
		Sleep(5000);
	
	}
	else if(flag==0 && steps!=1000) {
		save();
		is_success = 0;
		char s[20]; 
		cleardevice();
		settextcolor(RED);
		setbkmode(TRANSPARENT); 
		settextstyle(40, 0, _T("黑体"));
		loadimage(&failback, "..\\Images\\failbackground.png");
		putimage(0,0,&failback);
		loadimage(&fail1, "..\\Images\\failed1.png");
		putimage(280, 240, &fail1, NOTSRCERASE);
		loadimage(&fail, "..\\Images\\failed.png");
		putimage(280, 240,&fail, SRCINVERT);
		mciSendString("close backmusic", NULL, 0, NULL);
		mciSendString("close failmusic", NULL, 0, NULL);
		mciSendString("open ..\\Music\\failure.mp3 alias failmusic", NULL, 0, NULL);
		mciSendString("play  failmusic", NULL, 0, NULL);
		sprintf_s(s, "最终得分：%d", score);
		outtextxy(370,390,s);
		settextstyle(20, 0, _T("黑体"));
		outtextxy(430,450,"5秒后返回主菜单...");
		Sleep(5000);
	}
	else if (steps == 1000) {
		save();
		cleardevice();
		loadimage(&successback, "..\\Images\\successback.png", 1110, 680);
		putimage(-20, -10, &successback);
		mciSendString("close backmusic", NULL, 0, NULL);
		mciSendString("open ..\\Music\\success.mp3 alias successmusic", NULL, 0, NULL);
		mciSendString("play  successmusic", NULL, 0, NULL);
		settextcolor(YELLOW);
		setbkmode(TRANSPARENT);
		settextstyle(50, 0, _T("黑体"));
		outtextxy(360, 300, "恭喜您已通关！");
		settextstyle(30, 0, _T("黑体"));
		outtextxy(370, 400, "5秒后返回主菜单...");
		Sleep(5000);
	}
}





